import { VisitCardiologist, VisitDentist, VisitTherapist } from "../classes/Visit.js";


export function objectrender(obj){ const {
    doctor: doctorForm,
    purpose,
    description,
    urgency,
    fullname,
    id,
    age,
    normalpressure,
    bodymassindex,
    pastdiseasesofthecardiovascularsystem, 
    dateoflastvisit
  } = obj;

  switch (doctorForm) {
    case "cardiologist":
      new VisitCardiologist(
        purpose,
        description,
        urgency,
        fullname,
        id,
        normalpressure,
        bodymassindex,
        pastdiseasesofthecardiovascularsystem, 
       
      ).renderCard();
      break;
    case "dentist":
      new VisitDentist(
        purpose,
        description,
        urgency,
        fullname,
        id,
        dateoflastvisit
      ).renderCard();
      break;
      case "terapist":
        new VisitTherapist(
            purpose,
            description,
            urgency,
            fullname,
            id,
            age,
        )
        .renderCard();
        break;
  }}