import Form from "../classes/Form.js";
import Modal from "../classes/Modal.js";
import { startApp } from "./start_app.js";
export function entranceHendler () {
  
  const formStr = new Form ().renderLogin();
  const modal = new Modal("вход",formStr);
  modal.render();
  const form = document.querySelector('form');
  form.addEventListener('submit', function (event) {
  
    event.preventDefault();
    const emailInput = document.getElementById("exampleInputEmail1");
    const passwordInput = document.getElementById("exampleInputPassword1");
  
  
      fetch("https://ajax.test-danit.com/api/v2/cards/login", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email:emailInput.value, password: passwordInput.value })
      })
        .then(response => {if(response.ok){
  
  return response.text()
  }else {
    
    throw new Error('error incorrect user data')
  }
        })
        .then(token =>{
          modal.delete()
  

   localStorage.setItem("token",token )
   startApp()
  
        }
  
          
  
  
        ).catch(e =>alert(e.message));
  
 
  });
  
  }
