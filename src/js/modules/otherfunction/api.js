import { API } from "../helpers/constant.js"

export function getAllCards () {
  const token = localStorage.getItem("token");
   return fetch( API,{
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      })
        .then(response => response.json())
        .then(response => response)
      
}