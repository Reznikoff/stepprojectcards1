import Modal from "../classes/Modal.js";
import Form from "../classes/Form.js";
import { objectrender } from "../helpers/objectrender.js";
import { checkcards } from "./checkcards.js";
export function createvisit() {
  const modal = new Modal(
    "создать визит",
    `<div class="mb-3">
    <select class="form-select" id = "selectDoctor"  aria-label="выбор врача">
    <option selected>выбор врача</option>
    <option value="cardiologist">Cardiologist</option>
    <option value="dentist">Dentist</option>
    <option value="terapist">Terapist</option>
   </select>
   <div class="renderForm"></div>

    </div>`
  );
  modal.render();

  const selectDoctor = document.getElementById("selectDoctor");
  selectDoctor.addEventListener("change", (e) => {
    const wrapperForm = document.querySelector(".renderForm");
    wrapperForm.innerHTML = "";
    const formStr = new Form().renderCreate(e.target.value);

    wrapperForm.insertAdjacentHTML("afterbegin", formStr);

    createForm.onsubmit = async (e) => {
      e.preventDefault();
      const doctor = document.getElementById("selectDoctor").value;

      const formData = new FormData(createForm);
      const object = { doctor };
      for (const [key, value] of formData) {
        object[key] = value;
      }
      const token = localStorage.getItem("token");
      let response = await fetch("https://ajax.test-danit.com/api/v2/cards", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(object),
      });

      let result = await response.json();
     
objectrender(result)
modal.delete();
      checkcards()

    
    };
  });
}
