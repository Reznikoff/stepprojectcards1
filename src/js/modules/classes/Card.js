const API = "https://ajax.test-danit.com/api/json/posts/"

// прописуємо конструктор щоб наповнити  данними
class Card {constructor(title, textContent, name, lastName, email, postId){
    // привязіваем наша данніе до this
    this.title = title;
    this.name = name;
    this.textContent = textContent;
    this.lastName = lastName;
    this.email = email;
    this.postId = postId;
}

render(){
    const root = document.getElementById("root");
    root.insertAdjacentHTML("afterbegin", `<div id="${this.postId}">
    <div class="userInfo">
       <div><h3 class="name">${this.name}|</h3><h3 class="lastName">${this.lastName}</h3>
       </div><span class="email">${this.email}</span><button class="closeIcon">x</button>
    </div>
    <h2 class="title">${this.title}</h2>
    <p class="textContent">${this.textContent}</p>

</div>` )
}

deleteCard(){
    const cardWrapper = document.getElementById(this.postId);
    const closeIcon = cardWrapper.querySelector(".closeIcon");
    closeIcon.onclick = () => {
    fetch (API+this.postId,{method: "DELETE"})
    .then(res=>{
if(res.ok){
    cardWrapper.remove()
}else{
    throw new Error("Немає поста с таким id")
}
    




    }).catch((e) => {
console.log(e.message);
    })

    }
}





}
export default Card
