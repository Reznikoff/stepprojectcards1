import { API } from "../helpers/constant.js";
import { checkcards } from "../otherfunction/checkcards.js";

export class Visit {
  constructor(
    purpose = "",
    description = "",
    urgency = "",
    fullName = "",
    id = ""
  ) {
    this.purpose = purpose;
    this.description = description;
    this.urgency = urgency;
    this.fullName = fullName;
    this.id = id;
  }

  renderInputs() {
    return `  <label for="purposeVisitF" class="form-label">Цель визита</label>
    <input type="text" name="purpose" value = '${this.purpose}' class="form-control" id="purposeVisitF" aria-describedby="emailHelp"> </div>
   <div class="mb-3">
    <label for="descriptionF" class="form-label">Краткое описание визита</label>
    <input type="text" name="description" value = '${this.description}' class="form-control" id="descriptionF">
   </div>
   <div class="mb-3">
   <select class="form-select" name="urgency" aria-label="Срочность визита">
   <option selected>Срочность визита</option>
   <option value="обычная">обычная</option>
   <option value="приоритетная">приоритетная</option>
   <option value="неотложная">неотложная</option>
  </select>
   </div>
   <div class="mb-3">
    <label for="fullName" class="form-label">ФИО</label>
    <input type="text"  name="fullname" value = '${this.fullName}' class="form-control" id="fullName">
   </div>`;
  }
  renderCard(data) {
    const root = document.getElementById("root");
    root.insertAdjacentHTML(
      "afterbegin",
      `<div class ="card" id="${this.id}">

      <div class="card-body">
      <button class="btn-close closeIcon"></button>
      <ul class="card__list list-group list-group-flush">
      <li class="list-group-item">
      <h3 class="title ">цель визита</h3>
    <p class="card-footer value">${this.purpose}</p>
      </li>

      <li class="list-group-item"> 
      <h3 class="title">краткое описание визита</h3>
<p class="card-footer value">${this.description}</p></li>
      <li class="list-group-item">
      <h3 class="title">срочность</h3>
<p class="card-footer value">${this.urgency}</p></li>
<li class="list-group-item"><h3 class="title">ФИО</h3>
<p class="card-footer value">${this.fullName}</p> </li>
<button class="button">показать больше</button>
    <div class="hideblock active"> ${data}</div>
    </ul>

</div>
    </div>`
    );


    this.deleteCard()
    this.showmore()
  }


showmore(){
    const cardWrapper = document.getElementById(this.id);
    const closeIcon = cardWrapper.querySelector(".button")

    closeIcon.onclick =() => {

const hideblock = closeIcon.nextElementSibling
 hideblock.classList.toggle("active")
 if (hideblock.classList.contains("active")){
closeIcon.textContent = "показать больше"


 }else  {
    closeIcon.textContent = "скрыть"
 }

    }
}
  deleteCard(){
    const token = localStorage.getItem("token");
    
    const cardWrapper = document.getElementById(this.id);
    const closeIcon = cardWrapper.querySelector(".closeIcon");
    closeIcon.onclick = () => {
    fetch (API+this.id,{method: "DELETE",headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      }})
    .then(res=>{
if(res.ok){
    cardWrapper.remove()
   
}else{
    throw new Error("Немає картки з таким id")
}

    }).catch((e) => {
console.log(e.message);
    }).finally(()=> checkcards() )

    }
}
}

export class VisitTherapist extends Visit {
  constructor(
    purpose = "",
    description = "",
    urgency = "",
    fullName = "",
    id = "",
    age = ""
  ) {
    super(purpose, description, urgency, fullName, id), (this.age = age);
  }
  renderCard() {
  super.renderCard(`<li class="list-group-item"><h3 class="title">Возраст</h3>
  <p class="card-footer value">${this.age}</p></li>`)

   
    
  }

  renderInputs() {
    return `${super.renderInputs()}
<div class="mb-3">
    <label for="AgeF" class="form-label">возраст пациента</label>
    <input type="text" name="age" value = '${
      this.age
    }' class="form-control" id="AgeF">
   </div>
`;
  }
}

export class VisitCardiologist extends Visit {
  constructor(
    purpose = "",
    description = "",
    urgency = "",
    fullName = "",
    id ="",
    normalpressure = "",
    bodymassindex = "",
    pastdiseasesofthecardiovascularsystem = ""
  ) {
    super(purpose, description, urgency, fullName, id),
      (this.normalpressure = normalpressure);
    this.bodymassindex = bodymassindex;
    this.pastdiseasesofthecardiovascularsystem =
      pastdiseasesofthecardiovascularsystem;
  }

  renderCard() {
    const htmlString = `<li class="list-group-item">
    <h3 class="title">Нормальное давление</h3>
    <p class="card-footer value">${this.normalpressure}</p>
</li>
<li class="list-group-item">
    <h3 class="title">Индекс массы тела</h3>
    <p class="card-footer value">${this.bodymassindex}</p>
    </li>
    <li class="list-group-item">
    <h3 class="title">Перенесеннные заболевания</h3>
    <p class="card-footer value">${this.pastdiseasesofthecardiovascularsystem}</p>
    </li>
    `
    
    super.renderCard(htmlString)
  
     
      
    }

  renderInputs() {
    return `${super.renderInputs()}
        <div class="mb-3">
            <label for="NormalpressureF" class="form-label">нормальное давление</label>
            <input name="normalpressure" type="text" value = '${
              this.normalpressure
            }' class="form-control" id="NormalpressureF">
            <label for="BodymassindexF" class="form-label">индекс массы тела </label>
            <input name="bodymassindex"type="text" value = '${
              this.bodymassindex
            }' class="form-control" id="BodymassindexF">
            <label for="pastdiseasesofthecardiovascularsystemF" class="form-label">перенесенные заболевания сердечно-сосудистой системы </label>
            <input name="pastdiseasesofthecardiovascularsystem" type="text" value = '${
              this.pastdiseasesofthecardiovascularsystem
            }' class="form-control" id="pastdiseasesofthecardiovascularsystemF">
           </div>
        `;
  }
}

export class VisitDentist extends Visit {
  constructor(
    purpose = "",
    description = "",
    urgency = "",
    fullName = "",
    id = "",
    dateoflastvisit = ""
  ) {
    super(purpose, description, urgency, fullName, id);
    this.dateoflastvisit = dateoflastvisit;
  }

  renderCard() {
    const htmlString = `<li class="list-group-item">
    <h3 class="title">дата последнего визита</h3>
    <p class="card-footer value">${this.dateoflastvisit}</p></li>
    `
    super.renderCard(htmlString)
  
     
      
    }
  renderInputs() {
    return `${super.renderInputs()}
            <div class="mb-3">
             <label for="DateoflastvisitF" class="form-label">дата последнего визита</label>
            <input name="dateoflastvisit" type="text" value = '${
              this.dateoflastvisit
            }' class="form-control" id="DateoflastvisitF">
            </div>
            `;
  }
}
