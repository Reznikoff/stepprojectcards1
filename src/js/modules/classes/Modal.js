class Modal{
    constructor(title, content) {
this.title = title;
this.content = content;
    }

    delete(){
      document.querySelector('.modal').remove() ;
    }
render(){

    document.body.insertAdjacentHTML("afterbegin", `

<div class="modal modal-show" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">${this.title}</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      ${this.content}
      </div>
    </div>
  </div>
</div>
  `)
// навешиваем на крестик функцію видалення модального вікна 
const closeButton = document.querySelector(".btn-close");
closeButton.onclick = this.delete



}


}
export default Modal;
